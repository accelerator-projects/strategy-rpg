// import * as test from '@vue/test-utils'
import {mount} from '@vue/test-utils'
import Bishop from '@/components/Bishop.vue'


describe('Bishop', () => {

const wrapper = mount(Bishop,{propsData: {
  character: {player:true,style:'border:1px solid black'}
}})
wrapper.setData({
  info: {
    image: "pope-crown",
    archetype: {
      name: 'Bishop',
      health: 50,
      movement: 1,
      player: null,
      physicalDefence: 2,
      magicalDefence: 10,
      range: 1,
      damage: 30,
      movementToken: 1,
      damageType: 'mag',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  },
  baseHP: 50
})

it('shows the grass', () => {
  expect(wrapper.contains('.grassImg')).toBe(true)
})

it('shows the icon', () => {
  expect(wrapper.contains('.icon')).toBe(true)
})

wrapper.vm.$nextTick(function() {
  it("has a player value of true", () => {
    expect(wrapper.vm.info.archetype.player).toBe(true)
  })
})

})
