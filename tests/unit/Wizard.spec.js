// import * as test from '@vue/test-utils'
import {mount} from '@vue/test-utils'
import Wizard from '@/components/Wizard.vue'


describe('Wizard', () => {

const wrapper = mount(Wizard,{propsData: {
  character: {player:true,style:'border:1px solid black'}
}})
wrapper.setData({
    info: {
      image: "pointy-hat",
      archetype: {
        name: 'Wizard',
        health: 50,
        movement: 1,
        player: null,
        physicalDefence: 3,
        magicalDefence: 6,
        range: 2,
        damage: 22,
        damageType: 'mag',
        movementToken: 1
      },
      hasMoved: false,
      style: 'border:1px solid black',
      hasAttacked: false
    },
    baseHP: 50
})

it('shows the grass', () => {
  expect(wrapper.contains('.grassImg')).toBe(true)
})

it('shows the icon', () => {
  expect(wrapper.contains('.icon')).toBe(true)
})

it('creates the correct number of attack probabilities', () => {
  expect(wrapper.vm.attack(4,4)).toEqual([[{xPosition:2,yPosition:4},{xPosition:2,yPosition:3},{xPosition:1,yPosition:4},{xPosition:2,yPosition:5},{xPosition:3,yPosition:4}],[{xPosition:3,yPosition:3},{xPosition:3,yPosition:2},{xPosition:2,yPosition:3},{xPosition:3,yPosition:4},{xPosition:4,yPosition:3}],[{xPosition:3,yPosition:4},{xPosition:3,yPosition:3},{xPosition:2,yPosition:4},{xPosition:3,yPosition:5},{xPosition:4,yPosition:4}],[{xPosition:3,yPosition:5},{xPosition:3,yPosition:4},{xPosition:2,yPosition:5},{xPosition:3,yPosition:6},{xPosition:4,yPosition:5}],[{xPosition:4,yPosition:2},{xPosition:4,yPosition:1},{xPosition:3,yPosition:2},{xPosition:4,yPosition:3},{xPosition:5,yPosition:2}],[{xPosition:4,yPosition:3},{xPosition:4,yPosition:2},{xPosition:3,yPosition:3},{xPosition:4,yPosition:4},{xPosition:5,yPosition:3}],[{xPosition:4,yPosition:4},{xPosition:4,yPosition:3},{xPosition:3,yPosition:4},{xPosition:4,yPosition:5},{xPosition:5,yPosition:4}],[{xPosition:4,yPosition:5},{xPosition:4,yPosition:4},{xPosition:3,yPosition:5},{xPosition:4,yPosition:6},{xPosition:5,yPosition:5}],[{xPosition:4,yPosition:6},{xPosition:4,yPosition:5},{xPosition:3,yPosition:6},{xPosition:4,yPosition:7},{xPosition:5,yPosition:6}],[{xPosition:5,yPosition:3},{xPosition:5,yPosition:2},{xPosition:4,yPosition:3},{xPosition:5,yPosition:4},{xPosition:6,yPosition:3}],[{xPosition:5,yPosition:4},{xPosition:5,yPosition:3},{xPosition:4,yPosition:4},{xPosition:5,yPosition:5},{xPosition:6,yPosition:4}],[{xPosition:5,yPosition:5},{xPosition:5,yPosition:4},{xPosition:4,yPosition:5},{xPosition:5,yPosition:6},{xPosition:6,yPosition:5}],[{xPosition:6,yPosition:4},{xPosition:6,yPosition:3},{xPosition:5,yPosition:4},{xPosition:6,yPosition:5},{xPosition:7,yPosition:4}]])
})
wrapper.vm.$nextTick(function() {
  it("has a player value of true", () => {
    expect(wrapper.vm.info.archetype.player).toBe(true)
  })
})

})
