// import * as test from '@vue/test-utils'
import {mount} from '@vue/test-utils'
import Cavalry from '@/components/Cavalry.vue'


describe('Cavalry', () => {

const wrapper = mount(Cavalry,{propsData: {
  character: {player:true,style:'border:1px solid black'}
}})
wrapper.setData({
  info: {
    image: "horse-head",
    archetype: {
      name: 'Cavalry',
      health: 60,
      movement: 3,
      player: null,
      physicalDefence: 8,
      magicalDefence: 6,
      range: 1,
      damage: 25,
      movementToken: 3,
      damageType: 'phys',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  },
  baseHP: 60
})

it('shows the grass', () => {
  expect(wrapper.contains('.grassImg')).toBe(true)
})

it('shows the icon', () => {
  expect(wrapper.contains('.icon')).toBe(true)
})

it("has less damage when it hasn't moved", () => {
  wrapper.vm.getInfoData()
  expect(wrapper.vm.info.archetype.damage).toEqual(25)
})

it("has increased damage after moving", () => {
  wrapper.vm.info.hasMoved=true
  wrapper.vm.getInfoData()
  expect(wrapper.vm.info.archetype.damage).toEqual(35)
})

it("returns the value of its information", () => {
  expect(wrapper.vm.getInfoData()).toEqual(wrapper.vm.info)
})

wrapper.vm.$nextTick(function() {
  it("has a player value of true", () => {
    expect(wrapper.vm.info.archetype.player).toBe(true)
  })
})

})
