// import * as test from '@vue/test-utils'
import {mount} from '@vue/test-utils'
import Soldier from '@/components/Soldier.vue'


describe('Soldier', () => {

const wrapper = mount(Soldier,{propsData: {
  character: {player:true,style:'border:1px solid black'}
}})
wrapper.setData({
    info: {
      image: "crossed-swords",
      archetype: {
        name: 'Soldier',
        health: 80,
        movement: 2,
        player: null,
        physicalDefence: 10,
        magicalDefence: 4,
        range: 1,
        damage: 27,
        movementToken: 2,
        damageType: 'phys',
      },
      hasMoved: false,
      style: 'border:1px solid black',
      hasAttacked: false
    },
    baseHP: 80
})

it('shows the grass', () => {
  expect(wrapper.contains('.grassImg')).toBe(true)
})

it('shows the icon', () => {
  expect(wrapper.contains('.icon')).toBe(true)
})

wrapper.vm.$nextTick(function() {
  it("has a player value of true", () => {
    expect(wrapper.vm.info.archetype.player).toBe(true)
  })
})

})
