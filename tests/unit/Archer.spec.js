// import * as test from '@vue/test-utils'
import {mount} from '@vue/test-utils'
import Archer from '@/components/Archer.vue'


describe('Archer', () => {

const wrapper = mount(Archer,{propsData: {
  character: {player:false,style:'border:1px solid black'}
}})
wrapper.setData({
  info: {
    image: "high-shot",
    archetype: {
      name: 'Archer',
      health: 60,
      movement: 1,
      player: null,
      physicalDefence: 5,
      magicalDefence: 3,
      range: 3,
      damage: 35,
      movementToken: 1,
      damageType: 'phys',
    },
    hasMoved: false,
    style: 'border:1px solid black',
    hasAttacked: false
  },
  baseHP: 60
})

it('shows the grass', () => {
  expect(wrapper.contains('.grassImg')).toBe(true)
})

it('shows the icon', () => {
  expect(wrapper.contains('.icon')).toBe(true)
})

it('returns a valid list of all the possible cells it can attack', () => {
  expect(wrapper.vm.attack(4,4)).toEqual([[{"xPosition":1,"yPosition":4}],[{"xPosition":2,"yPosition":3}],[{"xPosition":2,"yPosition":5}],[{"xPosition":3,"yPosition":2}],[{"xPosition":3,"yPosition":6}],[{"xPosition":4,"yPosition":1}],[{"xPosition":4,"yPosition":7}],[{"xPosition":5,"yPosition":2}],[{"xPosition":5,"yPosition":6}],[{"xPosition":6,"yPosition":3}],[{"xPosition":6,"yPosition":5}],[{"xPosition":7,"yPosition":4}]])
})
wrapper.vm.$nextTick(function() {
  it("has a player value of true", () => {
    expect(wrapper.vm.info.archetype.player).toBe(false)
  })
  it("renders a blue version of the icon", () => {
    expect(wrapper.vm.info.image.is('high-shot-blue')).toBe(true)
  })
})

})
