import {mount} from '@vue/test-utils'
import Cell from '@/components/Cell.vue'
import Wizard from '@/components/Wizard.vue'
import Piece from '@/components/Piece.vue'
import Soldier from '@/components/Soldier.vue'
import Cavalry from '@/components/Cavalry.vue'
import Archer from '@/components/Archer.vue'
import Bishop from '@/components/Bishop.vue'

describe('Cell', () => {

  const wrapper = mount(Cell)
  wrapper.setData({
    piece: 'Piece',
    cell: {player:true,style:'border:1px solid black'},
    oldInfo:'default',
    newInfo: 'default',
    forRerender:0,
  })
  let bishops = wrapper.findAll(Bishop)
  for(let i=0;i<bishops.length;i++)
  {
    bishops.at(i).setData({
      info: {
        image: "pope-crown",
        archetype: {
          name: 'Bishop',
          health: 50,
          movement: 1,
          player: null,
          physicalDefence: 2,
          magicalDefence: 10,
          range: 1,
          damage: 30,
          movementToken: 1,
          damageType: 'mag',
        },
        hasMoved: false,
        style: 'border:1px solid black',
        hasAttacked: false
      },
      baseHP: 50
    })
  }
  let archers = wrapper.findAll(Archer)
  for(let i=0;i<archers.length;i++)
  {
    archers.at(i).setData({
      info: {
        image: "high-shot",
        archetype: {
          name: 'Archer',
          health: 60,
          movement: 1,
          player: null,
          physicalDefence: 5,
          magicalDefence: 3,
          range: 3,
          damage: 35,
          movementToken: 1,
          damageType: 'phys',
        },
        hasMoved: false,
        style: 'border:1px solid black',
        hasAttacked: false
      },
      baseHP: 60
    })
  }
  let cavalrys = wrapper.findAll(Cavalry)
  for(let i=0;i<cavalrys.length;i++)
  {
    cavalrys.at(i).setData({
      info: {
        image: "horse-head",
        archetype: {
          name: 'Cavalry',
          health: 60,
          movement: 3,
          player: null,
          physicalDefence: 8,
          magicalDefence: 6,
          range: 1,
          damage: 25,
          movementToken: 3,
          damageType: 'phys',
        },
        hasMoved: false,
        style: 'border:1px solid black',
        hasAttacked: false
      },
      baseHP: 60
    })
  }
  let soldiers = wrapper.findAll(Soldier)
  for(let i=0;i<soldiers.length;i++)
  {
    soldiers.at(i).setData({
      info: {
        image: "crossed-swords",
        archetype: {
          name: 'Soldier',
          health: 80,
          movement: 2,
          player: null,
          physicalDefence: 10,
          magicalDefence: 4,
          range: 1,
          damage: 27,
          movementToken: 2,
          damageType: 'phys',
        },
        hasMoved: false,
        style: 'border:1px solid black',
        hasAttacked: false
      },
      baseHP: 80
    })
  }
  let wizards = wrapper.findAll(Wizard)
  for(let i=0;i<wizards.length;i++)
  {
    wizards.at(i).setData({
      info: {
        image: "pointy-hat",
        archetype: {
          name: 'Wizard',
          health: 50,
          movement: 1,
          player: null,
          physicalDefence: 3,
          magicalDefence: 6,
          range: 2,
          damage: 22,
          damageType: 'mag',
          movementToken: 1
        },
        hasMoved: false,
        style: 'border:1px solid black',
        hasAttacked: false
      },
      baseHP: 50
    })
  }
  let pieces = wrapper.findAll(Piece)
  for(let i=0;i<pieces.length;i++)
  {
    pieces.at(i).setData({
      piece: 'Piece',
      cell: {player:true,style:'border:1px solid black'},
      oldInfo:'default',
      newInfo: 'default',
      forRerender:0,
    })
  }

it('changes the cell to show a particular piece', () => {
  wrapper.vm.initialiseCell('Wizard',false)
  wrapper.vm.$nextTick(function() {
      expect(wrapper.find(Wizard).hasStyle('display','none')).toBe(false)
      expect(wrapper.vm.getInfoData().archetype.player).toEqual(false)
    })
})

it('returns information about a piece in relevant information format',async () => {
  wrapper.vm.initialiseCell('Wizard',false)

  await function next() { wrapper.vm.$nextTick(async function() {
    expect(wrapper.vm.cellInfo()).toEqual(expect.arrayContaining(['value1',expect.stringContaining('Health'),expect.stringContaining('Physical Defence'),expect.stringContaining('Magical Defence'),expect.stringContaining('Damage'),expect.stringContaining('Range'),'value2']))
    expect(typeof 'value1').toBe(null)
    expect (typeof 'value2').toBe(Object)
  })
}
  await this.next().resolves
})
})
